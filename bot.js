var twit = require('twit');
var config = require('./config.js');
var Twitter = new twit(config);
var tweetTimeout;

var tweetTemplates = [
  'Između ostalog, predsednik Vučić je vanredno visok čovek. ',
  'Suprotno uvreženom verovanju ',
  'Između ostalog, hvala predsedniku Vučiću jer je toliko lep čovek. ',
  'Uostalom, ako mislite da ste bolji, osnujte svoju stranku, pa da vas vidimo na izborima. ',
  'A ako ste mislili da je tamo sve med i mleko... '
];

function getRandomTemplate() {
  return tweetTemplates[Math.floor((Math.random() * 5))];
}

function getTopicTweets() {
  var params = {
    q: 'Vucic',
    result_type: 'recent',
    lang: 'en'
  }

  Twitter.get('search/tweets', params, function(error, data) {
    if(!error) {

      if (!(data.statuses.length > 0)) {
        console.log('Nema tvitova sa tim pojmom');
      }

      spinTheTwheel(data.statuses);

    } else {
      console.log('Greska: ' + ' - ' + error);
    }
  });
}

function tweetWithQuotedTweet(tweetContent, quotedTweetID, topicTweets) {
  var embedableTweet = 'https://twitter.com/statuses/' + quotedTweetID;
  var completeTweet = tweetContent + embedableTweet;
  Twitter.post('statuses/update', { status: completeTweet }, function(err, data, response) {
    if (response) {
      console.log(completeTweet + ' - tweeted just now.');
    }

    if (err) {
      console.log('There was an error ' + err);
      pruneTopicTweets(0, topicTweets, quotedTweetID);
    } else {
      pruneTopicTweets(1, topicTweets, quotedTweetID);
    }
  })
}

function getRandomTweetID(topicTweets) {

  var arrayLength = topicTweets.length;
  var randomNumber = Math.floor((Math.random() * (arrayLength - 1)));

  return topicTweets[randomNumber].id_str;
}

var tweetDatTweet = function() {
  getTopicTweets();
}

function pruneTopicTweets(status, topicTweets, quotedTweetID) {

  var tweetToRemove = -1;

  if (status === 1) {
    tweetToRemove = topicTweets.indexOf(quotedTweetID);
    topicTweets = topicTweets.splice(tweetToRemove, 1);
    console.log('pruned...' + topicTweets.length);

    spinTheTwheel(topicTweets);
  } else {
    spinTheTwheel(topicTweets);
  }
};

function spinTheTwheel(topicTweets) {
  var success = 0;
  var tweetID = -1;
  var tweetContent = '';

  tweetTimeout = setTimeout(function() {
    if (topicTweets.length < 1) {
      clearTimeout(tweetTimeout);
      getTopicTweets();
    } else {
      tweetID = getRandomTweetID(topicTweets);
      tweetContent = getRandomTemplate();

      tweetWithQuotedTweet(tweetContent, tweetID, topicTweets);
    }
  }, 6000);
}

tweetDatTweet();
